<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bookmark;

class BookmarksController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $bookmarks=Bookmark::where('user_id',auth()->user()->id)->get();
        return view('home',['bookmarks'=>$bookmarks]);
    }

    public function store(Request $req )
    {
        $this->validate($req,[
            'name'=>'required',
            'url'=>'required',
        ]);

        $bookmark=new Bookmark;
        $bookmark->name=$req->name;
        $bookmark->url=$req->url;
        $bookmark->description=$req->description;
        $bookmark->description=$req->description;
        $bookmark->user_id=auth()->user()->id;

        $bookmark->save();
        
        return redirect('/home')->with('success','Bookmark Added');
    }

    public function destroy($id)
    {
        $bookmark=Bookmark::find($id);
        $bookmark->delete();
        return;
    }
}
