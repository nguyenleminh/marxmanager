@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @include('inc.messages')
                    <button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#addModal" type="button" name="button">
                        Add Bookmark
                    </button>
                    <hr>
                    <h3>My Bookmark</h3>
                    <ul class="list-group">
                        @foreach ($bookmarks as $bookmark)
                            <li class="list-group-item clearfix"> 
                                <a href="{{ $bookmark->url }}" target="_blank" style="position:absolute;top:30%">
                                    {{ $bookmark->name }} 
                                    <span class="badge badge-secondary">{{ $bookmark->description }} </span>
                                </a>
                                <span class="float-right button-group">
                                <button data-id="{{ $bookmark->id }}" type="button" class="delete-bookmark btn btn-danger" name="button">
                                        <span class="glyphicon glyphicon-remove"></span>
                                        Deleted
                                    </button>
                                </span>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <form action=" {{ route('bookmark.store') }}" method="POST" >
                    @csrf
                    <div class="form-group">
                        <label>Book Name</label>
                        <input type="text" name="name" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Book URL</label>
                        <input type="text" name="url" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Website Description</label>
                        <textarea type="text" name="description" class="form-control">

                        </textarea>
                    </div>
                    <input type="submit" name="submit" value="Submit" class="btn btn-success">
                </form>
            </div>
            
          </div>
        </div>
</div>
@endsection
